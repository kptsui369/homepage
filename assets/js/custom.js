$(document).ready(function () {
	var selec = $("#numVisitors");
	// update Visitor number
	if(!sessionStorage["visitcount"]){
		$.post("server.php", {ip: 1},
			function(data){
				// data.visitors
				sessionStorage["visitcount"] = data.visitors;

				//$(".serverInfo").append("Visitors: " + sessionStorage["visitcount"]);
				//animateNumber(sessionStorage["visitcount"], 0, selec);
	animateNumber(0, sessionStorage["visitcount"], selec);
			}, "json");
	}
	else{
		//$(".serverInfo").append("Visitors: " + sessionStorage["visitcount"]);
		//animateNumber(sessionStorage["visitcount"], 0, selec);
	animateNumber(0, sessionStorage["visitcount"], selec);
	}
			
	
// headroom
    $(".headroom").headroom({
        "tolerance": 20,
        "offset": 50,
        "classes": {
            "initial": "animated",
            "pinned": "slideDown",
            "unpinned": "slideUp"
        }
    });
//da slider
	if($('#da-slider').length){
		$('#da-slider').cslider({
			autoplay: true,
			bgincrement: 0
		});
	}
//Set the carousel options
	$('#quote-carousel').carousel({
		pause: true,
		interval: 4000,
	});
// fancybox
	if($(".fancybox").length){
		$(".fancybox").fancybox();
	}
//isotope
    if ($('.isotopeWrapper').length) {
		$(window).load(function(){
			var $container = $('.isotopeWrapper');
			var $resize = $('.isotopeWrapper').attr('id');
			// initialize isotope
			$container.isotope({
				itemSelector: '.isotopeItem',
				resizable: false, // disable normal resizing
				masonry: {
					columnWidth: $container.width() / $resize 
				}
			});
			$("a[href='#top']").click(function () {
				$("html, body").animate({
					scrollTop: 0
				}, "slow");
				return false;
			});
			$('.navbar-inverse').on('click', 'li a', function () {
				$('.navbar-inverse .in').addClass('collapse').removeClass('in').css('height', '1px');
			});
			$('#filter a').click(function () {
				$('#filter a').removeClass('current');
				$(this).addClass('current');
				var selector = $(this).attr('data-filter');
				$container.isotope({
					filter: selector,
					animationOptions: {
						duration: 1000,
						easing: 'easeOutQuart',
						queue: false
					}
				});
				return false;
			});
			$(window).smartresize(function () {
				$container.isotope({
					// update columnWidth to a percentage of container width
					masonry: {
						columnWidth: $container.width() / $resize
					}
				});
			});
		});
    }

	$(".nav li").hover(function(){
        $(this).css("background-color", "#003366");
        }, function(){
        $(this).css("background-color", "#4775D1");
    });
	$("#visitorsInfo").hover(function(){
        $(this).css("background-color", "#003366");
		animateNumber(0, sessionStorage["visitcount"], selec);
        }, function(){
        $(this).css("background-color", "#4775D1");
    });
	$("#serverName").hover(function(){
        $(this).css("background-color", "#003366");
        }, function(){
        $(this).css("background-color", "#4775D1");
    });
	
	
	$("#contactForm").submit(function(){
		var contactName = $("#contactName").val();
		var contactEmail = $("#contactEmail").val();
		var contactMessage = $("#contactMessage").val();
		$.post("sendEmail.php", {name: contactName, email: contactEmail, message: contactMessage});
		alert("Your message has been sent! Thank you!!");
		$("#contactName").val("");
		$("#contactEmail").val("");
		$("#contactMessage").val("");
		return false;
	});
});

function animateNumber(startNum, endNum, selector){
	jQuery({counter: startNum}).animate({counter: endNum}, {
			duration: 1500,
			easing:'swing', // can be anything
			step: function() { // called on every step
				// Update the element's text with rounded-up value:
				selector.text(this.counter.toFixed(0));
			}
	});
}